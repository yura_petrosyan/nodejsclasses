module.exports = (sequelize, DataTypes) => {
  const lessonStudent = sequelize.define("lesson_student", {
    visit: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, 
  {
    timestamps: false,

    createdAt: false,

    updatedAt: false,
  });

  return lessonStudent
};