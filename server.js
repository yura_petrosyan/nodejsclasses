const express = require('express')
const db = require('./models')
const path = require('path')
const lesson = require('./routes/lesson')

const app = express()

const PORT = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/', lesson)

async function start() {
    try {
        await db.sequelize.sync()
        app.listen(PORT, () =>{})
    } catch (e) {
        console.log(e)
    }
}

start()

module.exports = app