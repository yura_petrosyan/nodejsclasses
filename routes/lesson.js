const { Router } = require('express')
const db = require('../models')
const Lesson = db.lesson
const Student = db.student
const Teacher = db.teacher
const { Op } = db.Sequelize

const router = Router()

router.get('/', async (req, res) => {
    try {
        let page = parseInt(req.query.page)
        let limit = 5
        if (req.query.lessonsPerPage) limit = parseInt(req.query.lessonsPerPage)
        const offset = page ? (page - 1) * limit : 0

        let lessonDate = {}
        let lessonStatus = {}
        let teacherId = {}
        let startedDate
        let endDate

        if (req.query.date) {
            if (/,/.test(req.query.date)) {
                const start = req.query.date.match(/.+(?=,)/)
                startedDate = start[0]
                const end = req.query.date.match(/(?<=,).+/)
                endDate = end[0]
            } else {
                startedDate = req.query.date
            }
            lessonDate = {
                [Op.and]: [
                    {
                        [Op.or]: [
                            { 'date': { [Op.between]: [startedDate, endDate] } },
                            { 'date': { [Op.between]: [startedDate, startedDate] } },
                        ]
                    }
                ]
            };
        }

        if (req.query.status) {
            lessonStatus = {
                "status": req.query.status
            };
        }

        if (req.query.teacherIds) {
            teacherId = {
                'id': req.query.teacherIds.split(',')
            }
        }

        const lesson = await Lesson.findAll({
            where: [lessonDate, lessonStatus],
            limit: limit,
            offset: offset,
            attributes: ["id", "date", "title", "status"],
            include: [
                {
                    model: Student,
                    as: "students",
                    attributes: ["id", "name"],
                    through: {
                        attributes: ["visit"],
                    }
                },
                {
                    model: Teacher,
                    as: "teachers",
                    where: teacherId,
                    attributes: ["id", "name"],
                    through: {
                        attributes: [],
                    }
                },
            ]
        })

        let data = JSON.stringify(lesson, null, 2)
        let lessons = JSON.parse(data)

        lessons.forEach(element => {
            let visitCount = 0;
            element.students.forEach(element => {
                element.visit = element.lesson_student.visit
                delete element.lesson_student
                if (element.visit) {
                    visitCount++
                }
            })
            element.visitCount = visitCount
        });

        let results = lessons

        if (req.query.studentsCount) {
            if (/,/.test(req.query.studentsCount)) {
                const min = req.query.studentsCount.match(/\d(?=,)/)
                const max = req.query.studentsCount.match(/(?<=,)\d/)
                results = lessons.filter(function (element) {
                    if (element.students.length >= min[0] && element.students.length <= max[0]) {
                        return element
                    }
                })
            } else {
                results = lessons.filter(function (element) {
                    if (element.students.length == req.query.studentsCount) {
                        return element
                    }
                })
            }
        }

        res.status(200).json(results)
    } catch (e) {
        res.status(400).json({
            message: e
        })
    }
})


router.post('/lessons', async (req, res) => {
    try {
        let lessonsId = []
        const info = req.body
        const copyInfo = Object.assign({}, info)
        let year = 0

        async function addDays(date, days, weekEnd) {
            const result = new Date(date)
            result.setDate(result.getDate() + days)
            year = (new Date(result) - new Date(info.firstDate)) / 1000 / 3600 / 24
            if (weekEnd) {
                copyInfo.firstDate = result
            }
            const newLesson = await Lesson.create({
                title: info.title,
                date: result
            })

            info.teacherIds.forEach(element => {
                Teacher.findOne({ where: { id: element } })
                    .then(teacher => {
                        if (!teacher) return

                        Lesson.findOne({ where: { id: newLesson.id } })
                            .then(lesson => {
                                if (!lesson) return
                                teacher.addLesson(lesson)
                            })
                    })
            })

            return newLesson.id
        }

        let forStop
        let week
        let lesCount = 0

        if (info.lastDate) {
            const days = (new Date(info.lastDate) - new Date(info.firstDate)) / 1000 / 3600 / 24
            forStop = Math.ceil(days % 7)
            week = Math.ceil(days / 7)
        } else {
            forStop = info.lessonsCount % info.days.length
            week = Math.ceil(info.lessonsCount / info.days.length)
        }

        for (let i = 0; i < week; i++) {
            let weekEnd = false
            for (let w = 0; w < info.days.length; w++) {
                lesCount++
                if (lesCount >= 301 || year >= 364) {
                    break
                } else if (!i && !w) {
                    lessonsId.push(await addDays(copyInfo.firstDate, 0, weekEnd))
                } else {
                    if (i === week - 1 && w === forStop - 1) {
                        lessonsId.push(await addDays(copyInfo.firstDate, info.days[w], weekEnd))
                        break
                    } else if (w === info.days.length - 1) {
                        weekEnd = true
                        lessonsId.push(await addDays(copyInfo.firstDate, info.days[w], weekEnd))
                        const date = new Date(copyInfo.firstDate)
                        date.setDate(date.getDate() + 7 - info.days[w])
                        copyInfo.firstDate = date
                    } else {
                        lessonsId.push(await addDays(copyInfo.firstDate, info.days[w], weekEnd))
                    }
                }
            }
        }
        
        res.status(200).json(lessonsId)
    } catch (e) {
        res.status(400).json({
            message: e
        })
    }
})

module.exports = router