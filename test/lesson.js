const app = require('../server')
const chai = require('chai')
const chaiHttp = require('chai-http')

chai.should()
chai.use(chaiHttp)

describe('Lesson APIs', () => {

    describe("Test GET route /", () => {

        it("It should return lessons by default parameters", (done) => {
            chai.request(app)
                .get("/")
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                    done();
                });
        });

        it("It should return lessons by query parameters example 1", (done) => {
            chai.request(app)
                .get("/")
                .query({
                    date: '2019-01-01,2019-09-01',
                    status: '1',
                    teacherIds: '1,2',
                    studentsCount: '3,6',
                    page: '1',
                    lessonsPerPage: '10'
                })
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                    done();
                });
        });

        it("It should return lessons by query parameters example 2", (done) => {
            chai.request(app)
                .get("/")
                .query({
                    date: '2019-09-02',
                    status: '0',
                    teacherIds: '1',
                    studentsCount: '2',
                    page: '1',
                    lessonsPerPage: '10'
                })
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                    done();
                });
        });
    });

    describe("Test POST route /lessons", () => {

        it("It should POST a new lesson by lessonsCount", (done) => {
            const data = {
                teacherIds: [1, 2],
                title: 'Blue Ocean',
                days: [1, 2, 5],
                firstDate: '2019-09-20',
                lessonsCount: 3
            };
            chai.request(app)
                .post("/lessons")
                .send(data)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                    done();
                });
        });

        it("It should POST a new lesson by lastDate", (done) => {
            const data = {
                teacherIds: [1],
                title: 'Blue Ocean',
                days: [1, 3],
                firstDate: '2019-09-20',
                lastDate: '2020-12-31'
            };
            chai.request(app)
                .post("/lessons")
                .send(data)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.not.be.eq(0);
                    done();
                });
        });

    });
});


