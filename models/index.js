const config = require('../config/config')
const { Sequelize } = require('sequelize')

const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
    host: config.HOST,
    dialect: config.dialect,
    logging: false
})


const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.lesson = require("./lesson.model")(sequelize, Sequelize)
db.student = require("./student.model")(sequelize, Sequelize)
db.teacher = require("./teacher.model")(sequelize, Sequelize)
db.lessonStudent = require('./lessonStudent.model')(sequelize, Sequelize)

db.student.belongsToMany(db.lesson, {
    through: db.lessonStudent,
    as: "lessons",
    foreignKey: "student_id",
})
db.lesson.belongsToMany(db.student, {
    through: db.lessonStudent,
    as: "students",
    foreignKey: "lesson_id",
})
db.teacher.belongsToMany(db.lesson, {
    through: "lesson_teachers",
    as: "lessons",
    foreignKey: "teacher_id",
})
db.lesson.belongsToMany(db.teacher, {
    through: "lesson_teachers",
    as: "teachers",
    foreignKey: "lesson_id",
})

module.exports = db