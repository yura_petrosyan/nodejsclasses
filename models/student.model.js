module.exports = (sequelize, DataTypes) => {
    const Student = sequelize.define("student", {
        name: {
            type: DataTypes.STRING(10),
        },
    }, 
    {
        timestamps: false,

        createdAt: false,

        updatedAt: false,
    });

    return Student
};