module.exports = (sequelize, DataTypes) => {
    const Teacher = sequelize.define("teacher", {
        name: {
            type: DataTypes.STRING(10),
        },
    }, 
    {
        timestamps: false,

        createdAt: false,

        updatedAt: false,
    });

    return Teacher
};