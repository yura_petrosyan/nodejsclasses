module.exports = (sequelize, DataTypes) => {
    const Lesson = sequelize.define("lesson", {
        date: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING(100),
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
    }, 
    {
        timestamps: false,

        createdAt: false,

        updatedAt: false,
    });

    return Lesson
};